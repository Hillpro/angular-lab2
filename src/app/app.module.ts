import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient} from '@angular/common/http';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { routes } from './app.routes';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { RecipesComponent } from './components/recipes/recipes.component';
import { AuthService } from './services/auth.service';
import { NavComponent } from './components/nav/nav.component';
import { RecipeCreateComponent } from './components/recipe-create/recipe-create.component';
import { AuthGuardService } from './services/auth-guard.service';
import { RecipesService } from './services/recipes.service';
import { RecipeRowComponent } from './components/recipe-row/recipe-row.component';
import { RecipeViewComponent } from './components/recipe-view/recipe-view.component';
import { RequestService } from './services/request.service';
import { RequestInterceptorService } from './services/request-interceptor.service';

export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient);
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NotFoundComponent,
    RecipesComponent,
    NavComponent,
    RecipeCreateComponent,
    RecipeRowComponent,
    RecipeViewComponent,
  ],
  imports: [
    BrowserModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    RouterModule.forRoot(routes),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [
    AuthService,
    AuthGuardService,
    RecipesService,
    RequestService,
    {provide: HTTP_INTERCEPTORS, useClass: RequestInterceptorService, multi: true},
  ],
  bootstrap: [
    AppComponent,
  ]
})
export class AppModule { }

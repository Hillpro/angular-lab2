import { Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { RecipesComponent } from './components/recipes/recipes.component';
import { AuthGuardService } from './services/auth-guard.service';
import { RecipeViewComponent } from './components/recipe-view/recipe-view.component';

export const routes: Routes = [
  {path: '', component: LoginComponent },
  {path: 'recipes', canActivate: [AuthGuardService], children: [
    {path: '', component: RecipesComponent },
    {path: ':id', component: RecipeViewComponent}
  ]
  },
  {path: '**', component: NotFoundComponent },
];

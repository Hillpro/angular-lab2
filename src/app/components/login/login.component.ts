import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  welcomeMessage: string;
  username: string;
  password: string;
  loginButton: string;

  constructor(private authService: AuthService, private router: Router, public translate: TranslateService) {
  }

  ngOnInit() {
    if (this.authService.currentUser !== null) {
      this.router.navigate(['/recipes']);
    }

    this.welcomeMessage = this.translate.instant('welcomeMessage');

    this.translate.get(['login.username', 'login.password']).subscribe(
      translations => {
        this.username = translations['login.username'];
        this.password = translations['login.password'];
      }
    );

    this.loginButton = this.translate.instant('signin');
  }

  login(email: string, password: string) {

    this.authService.login(email, password).subscribe(
      user => {
        if (user) {
          this.router.navigate(['/recipes']);
        } else {
          alert(this.translate.instant('badCredentials'));
        }
      }
    );
  }
}

import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  loginButton: string;
  logoutButton: string;
  RecipesButton: string;

  constructor(private authService: AuthService, private router: Router, public translate: TranslateService) {
  }


  ngOnInit() {
    this.loginButton = this.translate.instant('navLogin');
  }

  get loggedIn(): boolean {
    return this.authService.currentUser != null;
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/']);
  }

  changeLang(lang: string) {
    this.translate.use(lang);
    localStorage.setItem('Lang', lang);
  }
}

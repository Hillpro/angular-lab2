import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, NgForm } from '@angular/forms';
import { RecipesService } from 'src/app/services/recipes.service';
import { Recipe } from 'src/app/models/recipe.model';

@Component({
  selector: 'app-recipe-create',
  templateUrl: './recipe-create.component.html',
  styleUrls: ['./recipe-create.component.css']
})
export class RecipeCreateComponent implements OnInit {

  @Output() readonly recipeCreate: EventEmitter<Recipe> = new EventEmitter();

  @ViewChild('form', {static: true}) formElement: NgForm;
  @ViewChild('name', {static: true}) nameField: ElementRef;

  recipeForm: FormGroup;

  constructor(formBuilder: FormBuilder, private recipesServices: RecipesService) {
    this.recipeForm = formBuilder.group(
      {
        name: new FormControl('', Validators.required),
        description: new FormControl('', Validators.required),
        category: new FormControl('', Validators.required),
      }
    );
  }

  ngOnInit() {
  }

  create() {
    if (this.recipeForm.valid) {
      const recipesValues = this.recipeForm.value;

      this.recipesServices.create(recipesValues.name, recipesValues.description, recipesValues.category).subscribe(
        newRecipe => {
          if (newRecipe) {
            this.recipeCreate.emit(newRecipe);
          } else  {
            alert('Could not complete create!');
          }
        }
      );

      this.formElement.resetForm();
      this.nameField.nativeElement.focus();
      this.recipeForm.controls.category.setValue('');
    }
  }
}

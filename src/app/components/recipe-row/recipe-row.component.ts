import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Recipe } from 'src/app/models/recipe.model';
import { RecipesService } from 'src/app/services/recipes.service';
import { Router } from '@angular/router';
import { RecipeCategory } from 'src/app/models/recipe_category.model';

@Component({
  // tslint:disable-next-line: component-selector
  selector: '[app-recipe-row]',
  templateUrl: './recipe-row.component.html',
  styleUrls: ['./recipe-row.component.css']
})
export class RecipeRowComponent implements OnInit {

  @Output() recipeDelete: EventEmitter<any> = new EventEmitter();
  @Input() recipe: Recipe;

  category: string;

  constructor(private recipesService: RecipesService, private router: Router) {
  }

  ngOnInit() {
    this.category = RecipeCategory[this.recipe.category];

    console.log(this.category);
  }

  delete() {
    this.recipesService.delete(this.recipe).subscribe(success => this.recipeDelete.emit());
  }

  view() {
    this.router.navigate(['/recipes/', this.recipe.id]);
  }
}

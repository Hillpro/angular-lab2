import { Component, OnInit } from '@angular/core';
import { Recipe } from 'src/app/models/recipe.model';
import { RecipesService } from 'src/app/services/recipes.service';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.css']
})
export class RecipesComponent implements OnInit {

  public _recipes: Recipe[] = [];

  constructor(private recipesService: RecipesService) {
    recipesService.recipes.subscribe(recipes => this._recipes = recipes);
  }

  ngOnInit() {
  }

  recipeCreated(newRecipe: Recipe) {
    this._recipes.unshift(newRecipe);
  }

  recipeDeleted() {
    this.recipesService.recipes.subscribe(recipes => this._recipes = recipes);
  }
}

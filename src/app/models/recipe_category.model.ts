export enum RecipeCategory {
  'Starter' = 1,
  'Main Course' = 2,
  'Dessert' = 3,
}

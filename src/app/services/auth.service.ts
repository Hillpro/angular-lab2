import { Injectable } from '@angular/core';
import { User } from '../models/user.model';
import { RequestService } from './request.service';
import { map} from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  private _currentUser: User;

  constructor(private requestService: RequestService) {
    this._currentUser = JSON.parse(localStorage.getItem('CurrentUser')) || null;
  }

  get currentUser(): User {
    return this._currentUser;
  }

  login(username: string, password: string): Observable<User> {

    return this.requestService.select('login', { username, password }).pipe(
      map(users => {
        if ( users && users.length === 1) {

          const userData = users[0];

          this._currentUser = new User(userData.id, userData.username);
          localStorage.setItem('CurrentUser', JSON.stringify(this._currentUser));

          return this._currentUser;
        } else {
          return null;
        }
      })
    );
  }

  logout() {
    this._currentUser = null;
    localStorage.setItem('CurrentUser', null);
  }
}

import { Injectable } from '@angular/core';
import { Recipe } from '../models/recipe.model';
import { RecipeCategory } from '../models/recipe_category.model';
import { AuthService } from './auth.service';
import { RequestService } from './request.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RecipesService {

  private _recipes: Recipe[];

  constructor(private requestService: RequestService, private authService: AuthService) {
    this._recipes = JSON.parse(localStorage.getItem('Recipes')) || [];
  }

  get recipes(): Observable<Recipe[]> {

    const _receivedRecipes: Recipe[] = [];

    return this.requestService.select('list', { userId : this.authService.currentUser.id}).pipe(
      map(recipes => {
        if (recipes) {

          for (const recipe of recipes) {
            _receivedRecipes.unshift(recipe);
          }
          this._recipes = _receivedRecipes;
          return _receivedRecipes;

        } else {
          return null;
        }
      })
    );
  }

  get(id: number): Observable<Recipe> {
    let _receivedRecipe: Recipe;

    return this.requestService.select('read', { id }).pipe(
      map(recipe => {
        if ( recipe && recipe.length === 1) {

          const recipeData = recipe[0];

          _receivedRecipe = new Recipe(
            recipeData.id,
            this.authService.currentUser.id,
            recipeData.name,
            recipeData.description,
            recipeData.category
          );

          return _receivedRecipe;
        } else {
          return null;
        }
      })
    );
  }

  create(name: string, description: string, category: RecipeCategory): Observable<Recipe> {

    const userId = this.authService.currentUser.id;

    return this.requestService.insert('create', { category, name, description, userId}).pipe(
      map (id => {
        if (id) {
          return new Recipe(id, userId, name, description, category);
        } else {
          return null;
        }
      })
    );
  }

  update(id: number, name: string, description: string, category: RecipeCategory): Observable<boolean> {

    return this.requestService.select('update', { category, name, description, id }).pipe(
      map(success => {
        return success;
      })
    );
  }

  delete(recipe: Recipe): Observable<boolean> {

    return this.requestService.delete('delete', { id: recipe.id });
  }
}

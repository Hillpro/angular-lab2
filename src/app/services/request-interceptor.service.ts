import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RequestInterceptorService implements HttpInterceptor {

  private readonly auth = btoa('contet:sW5MXgXN');

  constructor() { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log('RequestInterceptorService');

    const request = req.clone({headers: req.headers.set('auth', this.auth)});

    return next.handle(request).pipe(

      tap((response: HttpResponse<any>) => {
        // console.log(response);
      })

    );
  }
}
